<?php

Class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function loginValidation( $options ) {
        /** Variable creation */
        $userDetail = array();

        /** Input validation */
        if (empty($options['password']) || empty($options['username']))
            return $userDetail;

        $password = md5($options['password']);

        /** Get user detail information based on email id  * */
        $userDetail = $this->getLoginUserCredentialDetail($options);

        /** Check Password are match * */
        if (!empty($userDetail) && $password) {
            if ($userDetail['0']['password'] != $password) {
                $userDetail = array();
            }
        }
        return $userDetail;
    }

    function getLoginUserCredentialDetail( $options ) {
        $result = array();
        $username = (!empty($options['username'])) ? $options['username'] : '';
        if (empty($username)) {
            return $result;
        }
        $this->db->select('user.guid AS userGuid, user.password, user.username, user.status, user.email')
                ->where('user.email', $username);
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function loginRedirection($userRoleGuid) {
        /** Input validation  */
        if (empty($userRoleGuid)) {
            return false;
        }

        /** variable creation  */
        $path = '';

        switch ($userRoleGuid) {

            case MEMBER_ROLE_ID: {
                    $path = base_url() . 'index.php/matches/view/';
                    return $path;
                    break;
                }
            default: {
                    $path = 'index.php/logout';
                    return $path;
                    break;
                }
        }
    }

    function loginActivityEntry($userGuid, $logId = 1, $deviceType = 'Cloud') {
        $result = 0;
        if (empty($userGuid)) {
            return $result;
        }

        $loginDate = date(DATE_TIME_FORMAT);
        $activitydata = array(
            'activity_log_id' => $logId,
            'user_guid' => $userGuid,
            'client_date' => $loginDate,
            'activity_data1' => 'Login',
            'activity_data2' => 'Login',
            'active' => 1,
            'activity_comment' => 'Logged in Successfully',
            'device_type' => $deviceType,
            'created' => $loginDate,
            'created_by' => $userGuid,
        );
        $data = array('active' => 0);
        $this->db->where('user_guid', $userGuid);
        $result = $this->db->update('activity_log', $data);

        $insert = $this->db->insert('activity_log', $activitydata);
        return $insert;
    }
    
	function markers( $type ) {
        $result = array();
        if (empty($type)) {
            return $result;
        }
        $this->db->select('*')
                ->where('type', $type);
        $query = $this->db->get('markers');
        echo $this->db->last_query();exit;
        //return $query->result_array();
    }
    
    function getNearestMarkersLocationList( $requestData ){
    	$result = array();
    	if ( empty( $requestData )){
    		return $result;
    	}
    	
    	$Id		=	(!empty( $requestData['id'] )? $requestData['id']:'');
    	$lat		=	(!empty( $requestData['lat'] )? $requestData['lat']:'');
    	$lng		=	(!empty( $requestData['lng']) ? $requestData['lng']:'');
    	$product	=	(!empty( $requestData['product']) ? $requestData['product']:'');
    	$type		=	(!empty( $requestData['type']) ? $requestData['type']:'');
    	$brand		=	(!empty( $requestData['brand']) ? $requestData['brand']:'');
    	$banner		=	(!empty( $requestData['banner']) ? $requestData['banner']:'');
    	//$hills_ad	=	(!empty( $requestData['hills_ad']) ? $requestData['hills_ad']:'');
    	$radious	=	1000;
    	$flag = 0;
    	$this -> db -> select("*");
    
    	if (!empty( $lat ) && !empty( $lng )) {
    		$this -> db -> select(" ACOS( SIN( RADIANS( loc.`lat` ) ) * SIN( RADIANS( $lat ) ) + COS( RADIANS( loc.`lat` ) )* COS( RADIANS( $lat )) * COS( RADIANS( loc.`lng` ) - RADIANS( $lng )) ) * 6380 AS distance");
    		$flag = 1;
    		
    		if( $radious ){
    			$this -> db-> having('distance <=', $radious);
    		}
    	}
    
    	if( $product ){
    		$this -> db -> where('loc.product', $product);
    		$flag = 1;
    	}
    	if( $type ){
    		$this -> db -> where('loc.type', $type);
    		$flag = 1;
    	}
    	if( $brand ){
    		$this -> db -> where('loc.brand', $brand);
    		$flag = 1;
    	}
    	if( $banner ){
    		$this -> db -> where('loc.banner', $banner);
    		$flag = 1;
    	}
		if( $Id ){
			$this -> db -> where('loc.id', $Id);
			$flag = 1;
		}
    	
	    	$query = $this -> db -> get('markers AS loc');
	    	return $query->result_array();
    }

}

?>
