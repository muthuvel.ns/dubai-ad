<!DOCTYPE html>
<html>
  <head>
    <title>2I - SIGNAGE</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        overflow:hidden;
      }
      
	.gm-iv-address {
      display: none !important;
      }
      
     .gm-fullscreen-control{
      display: none !important;
      }
       .gm-compass{
      display: none !important;
      }
     .gmnoprint svg{
     display: none !important;
     }
     
    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
        <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">

    <script>

	function showDiv() {
		document.getElementById('welcomeDiv').style.display = "block";
	}
	
	function forgot(){
		document.getElementById('forgot').style.display = "block";
	}
	
	function guest() {
		$('#guest-form').submit();
	}
    </script>
<!--script type="text/javascript">
jQuery(window).load(function() { // makes sure the whole site is loaded
	jQuery("#status").fadeOut(); // will first fade out the loading animation
	jQuery("#preloader").delay(5000).fadeOut("slow"); // will fade out the white DIV that covers the website.
})

</script-->
       <script type="text/javascript">  
			                

                               
								   /*$(function () {
									   initMap();
                                        $('#status').show();
                                        setTimeout(function () {
                                            $('#load').hide();
                                        }, 60*60*60);
                                    });*/
  
        
           </script>
        
        <!-- PLUGIN INCLUSION  ------------------------------------- -->
        <!-- css -->
        <link href="<?php echo base_url();?>/assets/dist/css/introLoader.css" rel="stylesheet">
		<!-- jQuery, Helpers and jqueryIntrLoader -->
		<!--script src="<?php echo base_url();?>/assets/dist/helpers/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/helpers/spin.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/jquery.introLoader.js"></script-->      
        		<script>
			$(function() {
			//	$("#element").introLoader();
            });
		</script>
  </head>
 <!-- <div id="load">
	<div id="status">&nbsp;</div>
  </div>-->
  <body>
<!--div id="element" class="introLoading"></div-->
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
    <div id="map" style="height: 100%;" >    
			<div class="login_right contrst">
				<input type="submit" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color: #0C1842;" value="Login" onclick="showDiv()">
				<input type="submit" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color: #0C1842;" value="Guest" onclick="guest()">
			</div>
			
		    <div class="login-page contrst">
				<div class="form" style="display:none;" id="welcomeDiv">
					<form class="login-form" method="post" action="<?php echo base_url().'index.php/login'?>">
						<?php if ( !empty( $message ) ) { ?>
						<div style="text-align: center; color:red;"><?php echo $message;?></div>
						<?php }?>
						<div>
							<input type="text" name="username" placeholder="Enter Username" id="login" data-validation-optional= "false" data-validation="email" data-validation-error-msg="Please enter the valid email" value="">
						</div>
						<div>
							<input type="password" name="password" id="password" placeholder="Enter Password" data-validation="required" data-validation-error-msg="Please enter the password" value="">
						</div>
						<div>
							<button type="submit">Login</button>
						</div>
					</form>
					<div class="form-group" style="float:right;margin-top:10px;">
						<a href="#" class=''  data-toggle='modal' data-target='#myModal' style="color:#0C1842;">Forgot Password?</a>
					</div>       
		        </div>
			</div>
			<form class="login-form" id="guest-form" method="post" action="<?php echo base_url().'index.php/login?guest=1'?>">
			</form>
  
				  <!-- Modal -->
			  <div class="modal fade" id="myModal" role="dialog" style=""	>
				<div class="modal-dialog">
				
				  <!-- Modal content -->
				  <div class="modal-content" >
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title" style="font-size:18px !important;font-weight:bold;">Forgot Password</h4>
					</div>
					<div class="modal-body" id="modal_content1">			      
						<form method="POST" id="enquiry" action="">
							<div class="form-group row">
							 <label for="example-email-input" class="col-xs-3 col-form-label">Email</label>
							 <div class="col-xs-9">
							  <input class="form-control" type="email" name="email" value="" id="example-email-input" required>
							 </div>
							</div>
							<div class="modal-footer">
							  <button type="button" class="btn btn-default popup_button_default" data-dismiss="modal">Close</button>
							  <input type="submit" class="btn btn-success success popup_button_success" value="Submit">
							</div>
						</form>       
					</div>
				  </div>
				  
				</div>
			  </div>
			  
			  
			  
		</div>

<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script>
// Setup form validation
$.validate({
	 onError : function() {
		 $(":input.error:first").focus();
		 return false;
	    },
});

$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});
</script>

    <script>
   	  var baseurl = $("#baseurl").val();
 /*     var map;
      function initialize(mapstate) {
	    var pano;
		var latlng = new google.maps.LatLng(25.2676569,55.3549949);
		var panoOptions = {
		    position: latlng,
		    scrollwheel: true,
		    pov: {
			heading: 0,
			pitch: 0,
			zoom: 0,
		    }
		};
		map = new google.maps.StreetViewPanorama(
		    document.getElementById('map'), 
		    panoOptions);
		window.setInterval(function() {
		    var pov = map.getPov();
		    pov.heading += 0.1;
		    map.setPov(pov);
		}, 10);
        
      }
      
      google.maps.event.addDomListener(window, "load", initialize('empty'));*/

      var ROTATE_DEGS_PER_SEC = 60.0;           // Will take ~12 secs (360/60) for full revolution.
      var ROTATE_DELAY_SECS = 2.5;              // Wait 3 seconds to start.
      var ROTATE_FPS = 150.0;                   // Frames per second for rotate.
      var panoLastTouchTimestamp = Date.now();  // Timestamp pano loaded in milliseconds.
      var masterAnimate = true;                 // Set false to stop all animation.
      var panoHeadingAngle = 0;                 // Current heading of pano.

        /*
         * Click the map to set a new location for the Street View camera.
         */

        var map;
        var panorama;

        function initMap() {
          var berkeley = {lat: 25.1971501, lng:55.2740155};
          var sv = new google.maps.StreetViewService();

          panorama = new google.maps.StreetViewPanorama(document.getElementById('map'));


          // Set the initial Street View camera to the center of the map
          sv.getPanorama({location: berkeley, radius: 50}, processSVData);

          // Look for a nearby Street View panorama when the map is clicked.
          // getPanoramaByLocation will return the nearest pano when the
          // given radius is 50 meters or less.
          panoLastTouchTimestamp = Date.now();
        //  masterAnimate = true;
           
  	      $("#map").on("click", function () {
  	    	panoLastTouchTimestamp = 0;
  	    	if(masterAnimate==true){
  	    		masterAnimate = false;
  	    	}else{
  	    		masterAnimate = true;
  	    	}
  	      });
  	      
// 	      $("#pano").on("mouseleave", function () { 
// 	      	panoLastTouchTimestamp = 0;
// 	      });

        
      // Add animation which rotates our pano.
       window.setInterval(function() {
    	  
      	if (masterAnimate == false) {
      		return;
        }
        if (Date.now() < panoLastTouchTimestamp + ROTATE_DELAY_SECS * 1000) {
      		console.log(Math.floor(Date.now() / 1000));
      		return;
        }
        var pov = panorama.getPov();
        pov.heading += ROTATE_DEGS_PER_SEC / ROTATE_FPS;
        panoHeadingAngle = pov.heading;
        panorama.setPov(pov);
      }, 10000 / ROTATE_FPS);

      // Add a click listener which delays rotation.
      panorama.addListener('pov_changed', function() {
        // Check that change in POV is a user click, versus expected animation.
        var pov = panorama.getPov();
        if (panoHeadingAngle != pov.heading) {
      		panoLastTouchTimestamp = Date.now();  // Will cease animation for a while.
        }
      });
        
     }

        function processSVData(data, status) {
          if (status === 'OK') {
            var marker = new google.maps.Marker({
             // position: data.location.latLng,
              map: map,
              title: data.location.description
            });

            panorama.setPano(data.location.pano);
            panorama.setPov({
              heading: 360,
              pitch: 0
            });
            panorama.setVisible(true);

            marker.addListener('click', function() {
              var markerPanoID = data.location.pano;
              // Set the Pano to use the passed panoID.
              panorama.setPano(markerPanoID);
              panorama.setPov({
                heading: 360,
                pitch: 0
              });
              panorama.setVisible(true);
            });
          } else {
            console.error('Street View data not found for this location.');
          }
        }
        google.maps.event.addDomListener(window, 'load', initMap);
</script>
<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
     @import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background-color:rgba(255,255,255, 0.2);
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  border-radius: 10px;
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
  border-radius: 5px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #2D5D9E;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
  border-radius: 5px;
}
.form button:hover,.form button:active,.form button:focus {
  background: #2D5D9E;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
body {
    /*background: rgba(0, 0, 0, 0) url("assets/images/login_bg.jpg") repeat fixed 0 0 / 100% 100%;*/
    font-family: "Roboto",sans-serif;
}
.error-msg{
color: red;
}
 </style>
<style>


#preloader  {
     position: absolute;
     top: 0;
     left: 0;
     right: 0;
     bottom: 0;
     background-color: #fefefe;
     z-index: 99;
    height: 100%;
 }

#status  {
     width: 200px;
     height: 200px;
     position: absolute;
     left: 50%;
     top: 50%;
     background-image: url(assets/images/ajax-loader.gif);
     background-repeat: no-repeat;
     background-position: center;
     margin: -100px 0 0 -100px;
 }

</style>
</body>
</html>

  

 
