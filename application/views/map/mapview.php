<?php 
$lat = (!empty($latlong['lat'])?$latlong['lat']:'25.1943209');
$lng = (!empty($latlong['lng'])?$latlong['lng']:'55.2742231');
?>

<!DOCTYPE html>
<html>
  <head>
    <title>2I - SIGNAGE</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        overflow:hidden;
        font-family:inherit !important;
      }
       .gm-iv-address {
      display: none !important;
      }
   /*.gm-fullscreen-control{
      display: none !important;
      }
       .gm-compass{
      display: none !important;
      }*/
     .gmnoprint svg{
     display: none !important;
     }

    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
        <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/select2/select2.min.css';?>">
    <script src="<?php echo base_url().'assets/js/select2/select2.min.js'?>"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/font-awesome/css/font-awesome.min.css';?>"> 
    <script>
    $(document).ready(function(){
		 $(".js-example-basic-single").select2();
		 $(".js-example-data-array-selected").select2();
		 $('.cycle').cyclotron();
		 $("#brand").select2({
	            data: <?php echo (!empty($brand) ? $brand : NULL) ?>
	       });
	       
	     
		// var lat = <?php //echo $lat;?>;
		// var lng = <?php //echo $lng;?>;

		 $("#type").val(<?php echo (!empty($requestData['type']) ? $requestData['type'] : 0); ?>).trigger("change");
		// $("#product").val(<?php //echo (!empty($requestData['product']) ? $requestData['product'] : 0); ?>).trigger("change");
		 $("#brand").val(<?php echo (!empty($requestData['brand']) ? $requestData['brand'] : 0); ?>).trigger("change");
		 $("#banner").val(<?php echo (!empty($requestData['banner']) ? $requestData['banner'] : 0); ?>).trigger("change");
	});
    </script>

  </head>
  <body  onload="init()">
		
  <input type="hidden" id="marker" value='<?php echo (!empty($marker)?json_encode($marker):''); ?>'/>
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
  <input type="hidden" id="lat" value="<?php echo $lat;?>"/>
  <input type="hidden" id="lng" value="<?php echo $lng;?>"/>



  <div style="height:100%" id="full_map">
  <div id="pano" style='background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%; '></div>
  <!-- <div id="cycle" class='cycle' style='display:none;background-image:url(<?php echo base_url().'assets/images/'.(!empty($marker[0]['address'])?$marker[0]['address']:'')?>); background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%;'>-->
  <div id="cycle" class='cycle' style='display:none;background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%;'>
	<iframe src="" id="frame" width="100%" height="100%">
	</iframe>
	</div>
	<!-- Left content start -->	
    <div class="pull-left contrst topaction animated bounceInLeft" id="left_content">
		<div class="title" style="">
			<div style='float:right;'>
				<button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal1' title='Enquiry'><i class='glyphicon glyphicon-euro'></i></button>  
				<button type='button' class='btn btn-info btn-lg' id="modal" onclick='modalData("<?php echo (!empty($marker[0]['id'])?$marker[0]['id']:''); ?>")' title='Information'><i class='glyphicon glyphicon-info-sign'></i></button>
			</div>
				<h1 id="title" style=""><?php echo (!empty($marker[0]['name'])?$marker[0]['name']:''); ?>  </h1>
				<h5 id="description" style="text-align:justify;line-height: 1.5em;"> <?php echo (!empty($marker[0]['description'])?$marker[0]['description']:''); ?> </h5>
		</div>
    </div>
    
    <div class="left-bottom contrst " style="top:-10%;">
		<a href="<?php echo base_url().'index.php/map';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color:#fff">Home</a>
		<a href="<?php echo base_url().'index.php/logout';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5)  ;color:#fff">Logout</a>
		<!-- <a href="<?php //echo base_url().'index.php/map';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color:#fff">Back</a> -->
		  <span id="map_btn">
			<div class="pull-right" ><button id="show_map" class="btn btn-default " style="background-color:rgba(255, 255, 255, 0.5)  ;color:#fff; right:30px; float:right; ">Map</button> </div>
		  </span>

	</div>
    
	<!-- Left content End -->	
    
    <!-- Right content Start -->
    <div class="pull-right contrst topaction" id="map-content">
		<div class="options">
			
			<input type="hidden" id="lat" name="start_lat" value=""/>
			<input type="hidden" id="lng" name="start_lng" value=""/>
			<div id="map2" style=""></div>    
			<div class="block" id="map_form">
			<form id="search-form" method="post" action="" class="text_cont">
			  <div class="form-group col-md-6 new_form_group">
				<label for="location">Location</label>
				<select class="form-control js-example-basic-single" name="type" id="type">
				  <option value="0">-- Select --</option>
				  <option value="1">Sheikh Zayed Road</option>
				  <option value="2">JBR</option>
				  <option value="3">Burj Khalifa</option>
				  <option value="4">Diera Creek</option>
				</select>
			  </div>
			  <div class="form-group col-md-6 new_form_group">
				<label for="product">Category</label>
				<select class="form-control js-example-basic-single" name="product" id="product" onchange="cityInfo1(this.value)">
					<?php 						$product1 = $product2 = $product3 = $product4 = '';
						if($requestData['product'] == 1) { $product1 = "selected"; }
						if($requestData['product'] == 2) { $product2 = "selected"; }
						if($requestData['product'] == 3) { $product3 = "selected"; }
						if($requestData['product'] == 4) { $product4 = "selected"; }
					 ?>
				  <option value="0">-- Select --</option>
				  <option <?php echo $product1; ?> value="1">Fashion</option>
				  <option <?php echo $product2; ?> value="2">Sports</option>
				  <option <?php echo $product3; ?> value="3">Electronics</option>
				  <option <?php echo $product4; ?> value="4">Home Appliances</option>
				  <?php /*if ($requestData['product']) { ?>
                       <option value="<?php echo $requestData['product'] ?>" selected="selected"> <?php echo $productname ?> </option>
                  <?php } */?>
				</select>
			  </div>
			  <div class="form-group col-md-6 new_form_group">
				<label for="brand">Brand</label>
				<select class="form-control js-example-data-array-selected" name="brand" id="brand">
				  <option value="0">-- Select --</option>
				  <?php if ($requestData['brand']) { ?>
                       <option value="<?php echo $requestData['brand'] ?>" selected="selected"> <?php echo $brandname ?> </option>
                  <?php } ?>	
				</select>
			  </div>
			  <div class="form-group col-md-6 new_form_group">
				<label for="product">Type</label>
				<select class="form-control js-example-basic-single" name="banner" id="banner">
					<?php 	$banner1 = $banner2 = $banner3 = $banner4 = '';
						if($requestData['banner'] == 1) { $banner1 = "selected"; }
						if($requestData['banner'] == 2) { $banner2 = "selected"; }
					 ?>
				  <option value="0">-- Select --</option>
				  <option <?php echo $banner1; ?> value="1">Flex</option>
				  <option <?php echo $banner2; ?> value="2">Big Screen</option>
				</select>
			  </div>
			  <div class="form-group col-md-offset-3 col-md-6 new_form_group">
			  	<input type="submit" id="button" value="Search" class="btn btn-default btn_search" style=" padding-top: 3px !important; width:100%; float:right;margin-top:20px;background-color:#fff;border:1px solid #fff;">
				 <!--button type="submit" class="btn btn-default btn_search" style=" padding-top: 3px !important;">Search</button--> 
			  </div>
			</form>

    </div>
    </div>
    </div>
    <script>
    var baseurl = $("#baseurl").val();
	var markerpoint;
    var ROTATE_DEGS_PER_SEC = 60.0;           // Will take ~12 secs (360/60) for full revolution.
    var ROTATE_DELAY_SECS = 2.5;              // Wait 3 seconds to start.
    var ROTATE_FPS = 150.0;                 // Frames per second for rotate.
    var panoLastTouchTimestamp = Date.now();  // Timestamp pano loaded in milliseconds.
    var masterAnimate = true;                 // Set false to stop all animation.
    var panoHeadingAngle = 0;                 // Current heading of pano.
      /*
       * Click the map to set a new location for the Street View camera.
       */
      var map;
      var panorama;

    //  function initMap() {
        

        // Set up the map.
       /* map = new google.maps.Map(document.getElementById('map'), {
          center: berkeley,
          zoom: 16,
          streetViewControl: false
        });*/
   /*     var markers = [];
        var map2 = new google.maps.Map(document.getElementById('map2'), {
          center: berkeley,
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var Latlng = new google.maps.LatLng(25.197193, 55.274321);
        markers = new google.maps.Marker({
      	  position: Latlng,
           map: map2,
           label:''
       });*/


      
      
  //    }

    	function init(){

    		 var berkeley = {lat: <?php echo $lat;?>, lng: <?php echo $lng;?>};
    	       var sv = new google.maps.StreetViewService();

    	       panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));
    	       
    	        // Set the initial Street View camera to the center of the map
    	        sv.getPanorama({location: berkeley, radius: 50}, processSVData);

    	        // Look for a nearby Street View panorama when the map is clicked.
    	        // getPanoramaByLocation will return the nearest pano when the
    	        // given radius is 50 meters or less.
    	     /*   map1.addListener('click', function(event) {
    	          sv.getPanorama({location: event.latLng, radius: 50}, processSVData);
    	        });*/
    	         panoLastTouchTimestamp = Date.now();

    	    	$("#pano").on("click", function () {
    	    		panoLastTouchTimestamp = 0;
    	    	  	if(masterAnimate==true){
    	    	  		masterAnimate = false;
    	    	  	}else{
    	    	  		masterAnimate = true;
    	    	  	}
    	    	});

    	      
    	    // Add animation which rotates our pano.
    	     window.setInterval(function() {
    	    	 if (masterAnimate == false) {
    	    		return;
    	      	}
    	      	if (Date.now() < panoLastTouchTimestamp + ROTATE_DELAY_SECS * 1000) {
    	    		console.log(Math.floor(Date.now() / 1000));
    	    		return;
    	      	}
	    	      var pov = panorama.getPov();
	    	      pov.heading += ROTATE_DEGS_PER_SEC / ROTATE_FPS;
	    	      panoHeadingAngle = pov.heading;
	    	      panorama.setPov(pov);
    	    }, 10000 / ROTATE_FPS);

    	    // Add a click listener which delays rotation.
    	    panorama.addListener('pov_changed', function() {
    	      // Check that change in POV is a user click, versus expected animation.
    	      var pov = panorama.getPov();
    	      if (panoHeadingAngle != pov.heading) {
    	    		panoLastTouchTimestamp = Date.now();  // Will cease animation for a while.
    	      }
    	   		});
	   		
	    	var map1 = new google.maps.Map(document.getElementById("map2"), {
	    	    center: new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>),
	    	    zoom: 11,
	    	    mapTypeId: 'roadmap'
	    	  });
	
	    	if($('#marker').val()!=''){console.log($('#marker').val());
	    	    markerpoint    =   JSON.parse($('#marker').val());
	    	  }
			console.log(markerpoint);
	    	var infoWindow = new google.maps.InfoWindow;
	    	var lat_lng = new Array();
    	// Change this depending on the name of your PHP file
//    	 	$.post(baseurl+"index.php/map/viewmaps?type=1", function(data) {

//    	 	var markerpoint    =  jQuery.parseJSON(data);
    	     /** set marker point */
    		        var point1;                              
    	  		$.each(markerpoint, function(i, point1){
    	  	  		var name = point1.name;
    		  	  	var address = point1.address;
    		  	  	var id = point1.id;
    		  	 	var description = point1.description;
    		  		//var banner = point1.banner;
    		  		var flagurl = point1.flagurl

    		  	 //	var imageurl = baseurl+"assets/images/exe/1.exe";
    		  	 	if(i/2==0){
    		  	 	//	var imageurl = baseurl+"assets/images/exe/2.exe";
    		  	 	}
    		  		var html = '';//"<b>" + name + "</b><b><div style='float:right;'><button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal' title='Enquiry'><i class='glyphicon glyphicon-euro'></i></button>  <button type='button' class='btn btn-info btn-lg' onclick='modalData("+id+")' title='Information'><i class='glyphicon glyphicon-info-sign'></i></button></div></b> <br><br><br> <div class='cycle' style='background-image:url("+imageurl+"); background-position: -4067.3560813933063px 50%;height: 366px;cursor: move;width:654px;'></div>";
    				var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
    				lat_lng.push(myLatlng);
    				var marker = new google.maps.Marker({
    				  position: myLatlng,
    				      map: map1,
    				      icon: flagurl,
    				   //    label: 's',
    				 //     title: mar
    				});
    				bindInfoWindow(marker, map1, infoWindow, html, description, name, id, sv);
    	  		});
    	  		google.maps.event.addListener(map1, "click", function (event) {
    	  			$('#cycle').hide();
    	  			$('#pano').show();
    	  			 sv.getPanorama({location: event.latLng, radius: 50}, processSVData);
    	  		    

    	  		});
//    	 	});
    	}
    		function bindInfoWindow(marker, map1, infoWindow, html, description, name, id, sv) {
    		      google.maps.event.addListener(marker, 'click', function(eve) {console.log(eve.latLng)
    		      //  infoWindow.setContent(html);
    		     //   infoWindow.open(map1, marker);console.log(html);
    		      //sv.getPanorama({location: eve.latLng, radius: 50}, processSVData);
    		     $('#pano').hide();
    		     $('#map-content').hide();
    		     $('#cycle').show();
    		       // document.getElementById("cycle").style.backgroundImage = "url('"+html+"')";
    		    //   $('#modal').attr('onClick','modalData('+id+')');
    		        $('#description').html(description);
    		        $('#title').html(name);
    		        $('.cycle').cyclotron();
					//$('#full_map').slideToggle();	
					$('#left_content').show();
					$('#map_btn').show();
					$('#frame').load(baseurl+"index.php/map/frame");		
							
    		      });
    		}

    		function modalData(reqId){
    			$('#myModal').modal('show');
    			$.post(baseurl+"index.php/map/modaldata?type="+reqId, function(data) {
    				$("#modal_content").html(data);
    				
    			});
    		 }

    		  function processSVData(data, status) {
    		        if (status === 'OK') {
    		          var marker = new google.maps.Marker({
    		           // position: data.location.latLng,
    		            map: map,
    		            title: data.location.description
    		          });

    		          panorama.setPano(data.location.pano);
    		          panorama.setPov({
    		            heading: 270,
    		            pitch: 0
    		          });
    		          panorama.setVisible(true);

    		          marker.addListener('click', function() {
    		            var markerPanoID = data.location.pano;
    		            // Set the Pano to use the passed panoID.
    		            panorama.setPano(markerPanoID);
    		            panorama.setPov({
    		              heading: 270,
    		              pitch: 0
    		            });
    		            panorama.setVisible(true);
    		          });
    		        } else {
    		          alert('Street View data not found for this location.');
    		          console.error('Street View data not found for this location.');
    		        }
    		      }

    		/*	var panorama;
    		function initialize(lat, lng) {
//     			var lat = $('#lat').val();
//     			var lng = $('#lng').val();
    			//var latlng = $('#latlng').val();
    			console.log( lat);
    			console.log( lng);
    		    var location = new google.maps.LatLng(lat, lng);
    		    var panoramaOptions = {
    		        position: location,
    		        pov: {
    		        heading: 4,
    		            pitch: 10
    		        }
    		    };
    		    panorama = new  google.maps.StreetViewPanorama(document.getElementById('pano'),panoramaOptions);
    		    var move = true;
    		    $("#pano").on("mouseenter", function () {
    		        move = false;
    		    });

    		    $("#pano").on("mouseleave", function () {
    		        move = true;
    		    });

    		    var i = 0;
    		    window.setInterval(function () {
    		        panorama.setPov({
    		            heading: i,
    		            pitch: 10,
    		            zoom: 0
    		        });
    		        if (move) {
    		            i += 0.2;
    		        }
    		    }, 10);
    		}*/

    		google.maps.event.addDomListener(window, 'load', initMap);


    		function cityInfo1(reqData) {
                if (reqData == 0) {
					$("#brand").val('');
                   alert('please select category');
                    return false
                }
                $("#brand").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/map/brandlist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                        	alert('No brand available please select category');
                        } else {
                            $("#brand").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {
                        alert("connection error");
                        location.reload();
                    },
                });

                return false;
            }
    </script>
    <!-- <div  id="map1" style="display: block;width: 100%; height: 500px;" class="pano"></div> -->
  </body>
</html>
<script>
	$(document).ready(function(){
	$("#enquiry_popup").click(function(){ 
		$("#myModal").modal("hide");
		$("#myModal1").modal("show");
	});
	});
</script>
<!-- Modal -->
  <div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:18px !important;font-weight:bold;">Information</h4>
        </div>
        <div class="modal-body" id="modal_content">
          <img src="<?php echo base_url().'assets/img/loading.gif';?>" />
        </div>
        <div class="modal-footer">
		    <button type="button" class="btn btn-success success popup_button_success" id="enquiry_popup" >Enquiry</button>
          <button type="button" class="btn btn-default popup_button_default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- Modal -->
  <div class="modal" id="myModal1" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:18px !important;font-weight:bold;">Enquiry</h4>
        </div>
        <div class="modal-body" id="modal_content1">			      
        <form method="POST" id="enquiry" action="">
          <div class="form-group row">
          <label for="example-name-input" class="col-xs-3 col-form-label">Name<span class="import"><i class="fa fa-asterisk" aria-hidden="true"></i>
</span></label>
          <div class="col-xs-9">
            <input class="form-control" type="text" name="name" value="" id="example-name-input" placeholder="" required>
          </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-xs-3 col-form-label">Email<span class="import"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
            <div class="col-xs-9">
              <input class="form-control" type="email" name="email" value="" placeholder="" id="example-email-input" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="example-company-input" class="col-xs-3 col-form-label">Contact Number<span class="import"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
            <div class="col-xs-9">
              <input class="form-control" type="text" name="company" value="" placeholder="" id="example-company-input" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="enquiry" class="col-xs-3 col-form-label ">Enquiry Type<span class="import"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
            <div class="col-xs-9">
                <select class="form-control js-example-basic-single new_select" id="enquiry" placeholder="" required>
				  <option value="0">-- Select --</option>
				  <option value="1">Looking for Space</option>
				  <option value="2">Want to rent Space</option>
				  <option value="3">Others</option>
				</select>
            </div>
          </div>
          <div class="form-group row">
            <label for="exampleTextarea" class="col-xs-3 col-form-label">Comment</label>
            <div class="col-xs-9">
              <textarea class="form-control" name="message" id="exampleTextarea" rows="3" placeholder=""></textarea>
            </div>
          </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default popup_button_default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-success success popup_button_success" value="Submit">
        </div>
        </form>        
        </div>
      </div>
    </div>
  </div>
  
 
  
  <style>
.btn{  padding: 6px !important; }
body { background:#000 !important; }
.size {
    background: #094590 none repeat scroll 0 0;
    color: #fff;
    padding: 5px;
    text-align: center;
    width: 130px;

}
.size_content {
    padding: 5px 30px;
    text-align: justify;
    text-overflow: ellipsis;
    width: 31%;
}

.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
   background-color: #fff;
    font-family: Roboto;
    font-size: 10px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 100% !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#pac-input1 {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input1:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
.select2-container .new_select {
    height: 32px !important;
}
.select2-container .select2-selection--single .new_select {
    margin-top: 0px !important;
}
.form-group {
    margin-bottom: 15px;
}
.new_form_group{
margin-bottom: 0px !important;
}

.fa {
    font-size: 12px !important;
}      
         .animated {
            background-repeat: no-repeat;
            background-position: left top; 
            padding-top:95px;
            margin-bottom:60px;
            -webkit-animation-duration: 10s; 
            animation-duration: 4s; 
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both; 
         }
         
         @-webkit-keyframes bounceInLeft {
            0% {
               opacity: 0;
               -webkit-transform: translateX(-2000px);
            }
            60% {
               opacity: 1;
               -webkit-transform: translateX(30px);
            }
            80% {
               -webkit-transform: translateX(-10px);
            }
            100% {
               -webkit-transform: translateX(0);
            }
         }
         
         @keyframes bounceInLeft {
            0% {
               opacity: 0;
               transform: translateX(-2000px);
            }
            60% {
               opacity: 1;
               transform: translateX(30px);
            }
            80% {
               transform: translateX(-10px);
            }
            100% {
               transform: translateX(0);
            }
         }
         
         .bounceInLeft {
            -webkit-animation-name: bounceInLeft;
            animation-name: bounceInLeft;
         }


</style>

<script>
$(document).ready(function(){
/*
    $("#show_map").click(function(){
		location.reload();
        $("#map2").show();
        $("#map_form").show();
        $("#left_content").show();
    }); */
    $('#map_btn').hide(); 
    $("#show_map").click(function(){ 
    	//$('#full_map').show();
	     $('#cycle').hide();
	     $('#pano').show();
	     $('#map-content').show();
	    // $('#map_btn').hide(); 
// 		$("#show_map").css('display','block');
		//location.reload();
// 		$("#full_map").slideToggle();		
	});
});
</script>
