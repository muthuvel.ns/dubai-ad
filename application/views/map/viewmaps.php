<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      
    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
            <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
            <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
  </head>
  <body  onload="init()">
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
    <div  id="map1" style="width: 100%; height: 500px; " class="pano"></div>
  </body>
</html>
<script type="text/javascript">
var baseurl = $("#baseurl").val();
function init(){
var map1 = new google.maps.Map(document.getElementById("map1"), {
    center: new google.maps.LatLng(9.9545, 78.1404),
    zoom: 13,
    mapTypeId: 'roadmap'
  });

var infoWindow = new google.maps.InfoWindow;
var lat_lng = new Array();
// Change this depending on the name of your PHP file
	$.post(baseurl+"index.php/map/viewmaps?type=1", function(data) {

	var markerpoint    =  jQuery.parseJSON(data);
     /** set marker point */
	        var point1;                              
  		$.each(markerpoint, function(i, point1){
  	  		var name = point1.name;
	  	  	var address = point1.address;
	  	  var id = point1.id;
	  	  console.log(id);
	  	 	var imageurl = baseurl+"assets/images/"+address;
	  		var html = "<b>" + name + "</b><b><div style='float:right;'><button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal' title='Enquiry'><i class='glyphicon glyphicon-euro'></i></button>  <button type='button' class='btn btn-info btn-lg' onclick='modalData("+id+")' title='Information'><i class='glyphicon glyphicon-info-sign'></i></button></div></b> <br><br><br> <div class='cycle' style='background-image:url("+imageurl+"); background-position: -4067.3560813933063px 50%;height: 366px;cursor: move;width:654px;'></div>";
			var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
			lat_lng.push(myLatlng);
			var marker = new google.maps.Marker({
			  position: myLatlng,
			      map: map1,
			     // icon: point1.image,
			       label: 's',
			 //     title: mar
			});
			bindInfoWindow(marker, map1, infoWindow, html);
  		});
	});
}
	function bindInfoWindow(marker, map1, infoWindow, html) {
	      google.maps.event.addListener(marker, 'click', function() {
	        infoWindow.setContent(html);
	        infoWindow.open(map1, marker);
	        $('.cycle').cyclotron();
	      });
	}

	function modalData(reqId){
		$('#myModal').modal('show');
		$.post(baseurl+"index.php/map/modaldata?type="+reqId, function(data) {
			$("#modal_content").html(data);
			
		});
	 }

</script>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information</h4>
        </div>
        <div class="modal-body" id="modal_content">
          <?php echo base_url().'assets/img/loading.gif';?>...Loading
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <style>
.btn{  padding: 6px !important;  }
body { background:#000 !important; }
.size {
    background: #094590 none repeat scroll 0 0;
    color: #fff;
    padding: 5px;
    text-align: center;

}
.size_content {
    padding: 5px 30px;
    text-align: justify;
    text-overflow: ellipsis;
    width: 33%;
}

</style>
