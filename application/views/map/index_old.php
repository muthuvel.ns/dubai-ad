<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      
   .gm-iv-address {
      display: none !important;
      }
      
     .gm-fullscreen-control{
      display: none !important;
      }
       .gm-compass{
      display: none !important;
      }
     .gmnoprint svg{
     display: none !important;
     }
     
    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
        <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/select2/select2.min.css';?>">
    <script src="<?php echo base_url().'assets/js/select2/select2.min.js'?>"></script>
    <script>
    $(document).ready(function(){
		 $(".js-example-basic-single").select2();
	});
    </script>

  </head>
  <body>
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
    <div id="map" style="height: 100%;" >
	
	<!-- Left content start -->	
    <div class="pull-left contrst">
		<div class="title" >
			<h1> Burj Khalifa </h1>
			<h4 style="text-align:justify;">Indians own more than a hundred of 900 apartments in Dubai’s Burj Khalifa</h4>
			<h5 style="text-align:justify;">Burj Khalifa, the world's tallest tower and perhaps the poshest address in the Gulf, is fast acquiring a distinct Indian identity.</h5>
			<h5 style="text-align:justify;">Indians now own more than a hundred of 900 apartments in Dubai's 828-metre skyscraper, a sign of the growing financial power of the community in the West Asia.</h5>
			<h5 style="text-align:justify;">Developers have announced plans to build a new tower in Dubai to surpass the Burj Khalifa, currently the world's tallest building.</h5>
			<form id="search-form" method="post">
			  <div class="form-group col-md-6">
				<label for="location">Location</label>
				<select class="form-control js-example-basic-single" name="type" id="location">
				  <option value="0">-- Select --</option>
				  <option value="1">Dubai</option>
				  <option value="2">Sharaj</option>
				  <option value="3">Abu dhabi</option>
				  <option value="4">Al Ain</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="brand">Brand</label>
				<select class="form-control js-example-basic-single" name="brand" id="brand">
				  <option value="0">-- Select --</option>
				  <option value="1">Reebok</option>
				  <option value="2">Fast track</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="product">Product</label>
				<select class="form-control js-example-basic-single" name="product" id="product">
				  <option value="0">-- Select --</option>
				  <option value="1">Bag</option>
				  <option value="2">Watch</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
			  	<input type="submit" id="button" value="Search" class="btn btn-default btn_search" style=" padding-top: 3px !important;">
				<!-- <button type="submit" class="btn btn-default btn_search" style=" padding-top: 3px !important;">Search</button> -->
			  </div>
			</form>
		</div>
    </div>
	<!-- Left content End -->	
    
    <!-- Right content Start -->
    <div class="pull-right contrst">
		<div class="options">
			
			<input type="hidden" id="lat" name="start_lat" value=""/>
			<input type="hidden" id="lng" name="start_lng" value=""/>
			<div id="map2" style=""></div>    
			<div class="block" id="map_form">
					<input id="pac-input" class="controls" type="text" placeholder="Search Google Maps">
					<!--p style="color:#fff;font-size:10px;">Please press the "<b>Enter Button</b>" once enter the address</p-->
			</div>
		</div>

    </div>
    </div>
    <script>
    	var baseurl = $("#baseurl").val();

    	    function homestatus() {
    	        var data = $('#search-form').serialize();
    			if(data=='' || data==0){
    				return false;
    			}
    				
        		mapview( data );
        		return false;
    	    };

    		$(document).ready(function(){
        	    $("#search-form").submit(function(){
        	    	var data = $('#search-form').serialize();
        			if(data=='' || data==0){
        				return false;
        			}
        			$("#map1").show();
        			google.maps.event.addDomListener(window, "load", initialize("Maximize"));
            		mapview( data );
					minscreen();
					return false;
        	    });
        	});
        	
      var map;
      function initialize(mapstate) {
	    var pano;
		var latlng = new google.maps.LatLng(25.1943209,55.2742231);//25.2676569,55.3549949
		var panoOptions = {
		    position: latlng,
		    scrollwheel: true,
		    pov: {
			heading: 0,
			pitch: 0,
			zoom: 0,
		    }
		};
		map = new google.maps.StreetViewPanorama(
		    document.getElementById('map'), 
		    panoOptions);
		/*window.setInterval(function() {
		    var pov = map.getPov();
		    pov.heading += 0.2;
		    map.setPov(pov);
		}, 10);*/
        var homeControlDiv = document.createElement("div");
        var homeControl = RenderButton(homeControlDiv, map,mapstate);
        homeControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
      }
      
      google.maps.event.addDomListener(window, "load", initialize('empty'));
      
      function RenderButton(controlDiv, map, mapstate) {
          if(mapstate != 'empty'){
	        // Set CSS styles for the DIV containing the control
	        // Setting padding to 5 px will offset the control
	        // from the edge of the map
	        controlDiv.style.padding = "5px";
	        // Set CSS for the control border
			var controlUI = document.createElement("div");
	        controlUI.style.backgroundColor = "white";
	        controlUI.style.borderStyle = "solid";
	        controlUI.style.borderWidth = "2px";
	        controlUI.style.cursor = "pointer";
	        controlUI.style.textAlign = "center";
	        controlUI.title = "Click to set the map to Home";
	        controlDiv.appendChild(controlUI);
	        // Set CSS for the control interior
	        var controlText = document.createElement("div");
	        controlText.style.fontFamily = "Arial,sans-serif";
	        controlText.style.fontSize = "12px";
	        controlText.style.paddingLeft = "4px";
	        controlText.style.paddingRight = "4px";
	        //if(no==0)
	        controlText.innerHTML = mapstate;
	        controlUI.appendChild(controlText);
	        google.maps.event.addDomListener(controlUI, "click", function() {
	                if (controlText.innerHTML == "Maximize") { 
	                    fullscreen();
	                    controlText.innerHTML = "Minimize";
	                    google.maps.event.addDomListener(window, "load", initialize("Minimize"));
	                } else {
	                    minscreen();
	                    controlText.innerHTML = "Maximize";
	                    google.maps.event.addDomListener(window, "load", initialize("Maximize"));
	                }
	        });
         }
    }

 //   google.maps.event.addDomListener(window, "load", initialize("Maximize"));

//You can configure full screen and min screen implementation in some function like this,

function fullscreen() {
        var elem = document.getElementById("map");
        elem.style.width="100%";
        elem.style.height="100%";
}
 function minscreen() {
        var elem = document.getElementById("map");
        elem.style.width="10%";
        elem.style.height="10%";
 }
</script>
    <div  id="map1" style="display: block;width: 100%; height: 500px;" class="pano"></div>
  </body>
</html>
<script type="text/javascript">
function mapview( LatLng ){
	var map1 = new google.maps.Map(document.getElementById("map1"), {
	    center: new google.maps.LatLng(25.197193, 55.274321),
	    zoom: 6,
	    mapTypeId: 'roadmap'
	  });

	var infoWindow = new google.maps.InfoWindow;
	var lat_lng = new Array();
	// Change this depending on the name of your PHP file
		$.post(baseurl+"index.php/map/getlocation", LatLng, function(data) {

		var markerpoint    =  jQuery.parseJSON(data);
	     /** set marker point */
		        var point1;                              
	  		$.each(markerpoint, function(i, point1){
	  	  		var name = point1.name;
		  	  	var address = point1.address;
		  	  	var id = point1.id;
		  	  console.log(id);
		  	 	var imageurl = baseurl+"assets/images/"+address;
		  		var html = "<b>" + name + "</b><b><div style='float:right;'><button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal1' title='Enquiry' style='line-height: 0.333;'><i class='glyphicon glyphicon-euro'></i></button>  <button type='button' class='btn btn-info btn-lg' onclick='modalData("+id+")' title='Information' style='line-height: 0.333;'><i class='glyphicon glyphicon-info-sign'></i></button></div></b> <br><br><br> <div class='cycle' style='background-image:url("+imageurl+"); background-position: -4067.3560813933063px 50%;height: 366px;cursor: move;width:654px;'></div>";
				var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
				lat_lng.push(myLatlng);
				var marker = new google.maps.Marker({
				  position: myLatlng,
				      map: map1,
				     // icon: point1.image,
				       label: 's',
				 //     title: mar
				});
				bindInfoWindow(marker, map1, infoWindow, html);
	  		});
		});
}
function bindInfoWindow(marker, map1, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(html);
      infoWindow.open(map1, marker);
      $('.cycle').cyclotron();
    });
  }
function modalData(reqId){
	$('#myModal').modal('show');
	$.post(baseurl+"index.php/map/modaldata?type="+reqId, function(data) {
		$("#modal_content").html(data);
		
	});
 }


</script>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information</h4>
        </div>
        <div class="modal-body" id="modal_content">
          <img src="<?php echo base_url().'assets/img/loading.gif';?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enquiry</h4>
        </div>
        <div class="modal-body" id="modal_content1">			      
        <form method="POST" id="enquiry" action="">
          <div class="form-group row">
          <label for="example-name-input" class="col-xs-2 col-form-label">Name</label>
          <div class="col-xs-10">
            <input class="form-control" type="text" name="name" value="" id="example-name-input">
          </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-xs-2 col-form-label">Email</label>
            <div class="col-xs-10">
              <input class="form-control" type="email" name="email" value="" id="example-email-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-company-input" class="col-xs-2 col-form-label">Company</label>
            <div class="col-xs-10">
              <input class="form-control" type="text" name="company" value="" id="example-company-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="exampleTextarea" class="col-xs-2 col-form-label">Message</label>
            <div class="col-xs-10">
              <textarea class="form-control" name="message" id="exampleTextarea" rows="3"></textarea>
            </div>
          </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-success success" value="Submit">
        </div>
        </form>        </div>

      </div>
      
    </div>
  </div>
  <style>
.btn{  padding: 6px !important; }
body { background:#000 !important; }
.size {
    background: #094590 none repeat scroll 0 0;
    color: #fff;
    padding: 5px;
    text-align: center;

}
.size_content {
    padding: 5px 30px;
    text-align: justify;
    text-overflow: ellipsis;
    width: 33%;
}

.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
   background-color: #fff;
    font-family: Roboto;
    font-size: 10px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 100% !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#pac-input1 {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input1:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

</style>


<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

  var map2 = new google.maps.Map(document.getElementById('map2'), {
    center: {lat: 25.197193, lng: 55.274321},
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  //map2.controls[google.maps.ControlPosition.TOP_CENTER].push(input);


  // Bias the SearchBox results towards current map's viewport.
  map2.addListener('bounds_changed', function() {
    searchBox.setBounds(map2.getBounds());
  });


  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {

    var places = searchBox.getPlaces();
console.log(places);

    if (places.length == 0) {
      return;
    }

    var Latlng = new google.maps.LatLng(25.197193, 55.274321);
     markers = new google.maps.Marker({
  	  position: Latlng,
        map: map2,
        label:''
    });
    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    var LatLng ={};
 	// For each place, get the icon, name and location.
    places.forEach(function(place) {
	    
	 //	document.getElementById("lat").value = place.geometry.location.lat();
	 //	document.getElementById("lng").value = place.geometry.location.lng();
	 	LatLng = place.geometry.location.toJSON();
	 	console.log(LatLng);
	 	$("#map1").show();
		 mapview( LatLng );
		 minscreen();
		 google.maps.event.addDomListener(window, "load", initialize("Maximize"));
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map2,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map2.fitBounds(bounds);
  });
  // [END region_getplaces]



</script>    
