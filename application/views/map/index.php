<!DOCTYPE html>
<html>
  <head>
    <title>2I - SIGNAGE</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        overflow:hidden;
        font-family:inherit !important;
      }
      
       
    .gm-iv-address {
      display: none !important;
      }
   /*.gm-fullscreen-control{
      display: none !important;
      }
       .gm-compass{
      display: none !important;
      }*/
     .gmnoprint svg{
     display: none !important;
     }
     .form-group {
    margin-bottom: 0px !important;
	}
	label {
		margin-bottom: 0px !important;
	}
	.animated {
            background-repeat: no-repeat;
            background-position: left top; 
            padding-top:95px;
            margin-bottom:60px;
            -webkit-animation-duration: 10s; 
            animation-duration: 4s; 
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both; 
}
       
@-webkit-keyframes bounceInLeft {
            0% {
               opacity: 0;
               -webkit-transform: translateX(-2000px);
            }
            60% {
               opacity: 1;
               -webkit-transform: translateX(30px);
            }
            80% {
               -webkit-transform: translateX(-10px);
            }
            100% {
               -webkit-transform: translateX(0);
            }
}
         
@keyframes bounceInLeft {
            0% {
               opacity: 0;
               transform: translateX(-2000px);
            }
            60% {
               opacity: 1;
               transform: translateX(30px);
            }
            80% {
               transform: translateX(-10px);
            }
            100% {
               transform: translateX(0);
            }
}
         
.bounceInLeft {
            -webkit-animation-name: bounceInLeft;
            animation-name: bounceInLeft;
}
    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
        <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/select2/select2.min.css';?>">
    <script src="<?php echo base_url().'assets/js/select2/select2.min.js'?>"></script>
    <script>
    $(document).ready(function(){
		 $(".js-example-basic-single").select2();
	});
    </script>

  </head>
  <body>
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
    <div id="pano" style="height: 100%;" >    </div>

	
	<!-- Left content start -->	
    <div class="pull-left contrst animated bounceInLeft" style="top:-100%;">
		<div class="title" >
			<h1 style=""> Burj Khalifa </h1>
			<h5 style="text-align:justify;line-height: 1.5em;">Indians own more than a hundred of 900 apartments in Dubai’s Burj Khalifa</h5>
			<h5 style="text-align:justify;line-height: 1.5em;">Burj Khalifa, the world's tallest tower and perhaps the poshest address in the Gulf, is fast acquiring a distinct Indian identity.</h5>
			<h5 style="text-align:justify;line-height: 1.5em;">Indians now own more than a hundred of 900 apartments in Dubai's 828-metre skyscraper, a sign of the growing financial power of the community in the West Asia.</h5>
			<h5 style="text-align:justify;line-height: 1.5em;">Developers have announced plans to build a new tower in Dubai to surpass the Burj Khalifa, currently the world's tallest building.</h5>
		</div>
    </div>
	<!-- Left content End -->	
    
    <!-- Right content Start -->
    <div class="pull-right contrst" style="top:-100%;">
		<div class="options">
			
			<input type="hidden" id="lat" name="start_lat" value=""/>
			<input type="hidden" id="lng" name="start_lng" value=""/>
			<div id="map2" style=""></div>    
			<div class="block" id="map_form">
			<form id="search-form" method="post" action="<?php echo base_url().'index.php/map/mapview';?>" class="text_cont">
			   <div class="form-group col-md-6">
				<label for="location">Location</label>
				<select class="form-control js-example-basic-single" name="type" id="type">
				  <option value="0">-- Select --</option>
				  <option value="1">Sheikh Zayed Road</option>
				  <option value="2">JBR</option>
				  <option value="3">Burj Khalifa</option>
				  <option value="4">Diera Creek</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="product">Category</label>
				<select class="form-control js-example-basic-single" name="product" id="product" onchange="cityInfo1(this.value)">
				  <option value="0">-- Select --</option>
				  <option value="1">Fashion</option>
				  <option value="2">Sports</option>
				  <option value="3">Electronics</option>
				  <option value="4">Home Appliances</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="brand">Brand</label>
				<select class="form-control js-example-basic-single" name="brand" id="brand">
				  <option value="0">-- Select --</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="product">Type</label>
				<select class="form-control js-example-basic-single" name="banner" id="banner">
				  <option value="0">-- Select --</option>
				  <option value="1">Flex</option>
				  <option value="2">Big Screen</option>
				</select>
			  </div>
			  <div class="form-group col-md-offset-3 col-md-6">
			  	<input type="submit" id="button" value="Search" class="btn btn-default btn_search" style=" padding-top: 3px !important; width:100%; float:right;margin-top:20px;background-color:#fff;border:1px solid #fff;">
				 <!--button type="submit" class="btn btn-default btn_search" style=" padding-top: 3px !important;">Search</button--> 
			  </div>
			</form>
					<!--input id="pac-input" class="controls" type="text" placeholder="Search Google Maps"-->
					<!--p style="color:#fff;font-size:10px;">Please press the "<b>Enter Button</b>" once enter the address</p-->
			</div>
		</div>
    </div>
    <!-- Right content End -->

	<div class="left-bottom contrst" style="top:-10%;">
		<a href="<?php echo base_url();?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color:#fff">Home</a>
		<a href="<?php echo base_url().'index.php/logout';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5)  ;color:#fff">Logout</a>
	</div>

    <script>
    	var baseurl = $("#baseurl").val();

   /* 	    function homestatus() {
    	        var data = $('#search-form').serialize();
    			if(data=='' || data==0){
    				return false;
    			}
    				
        		mapview( data );
        		return false;
    	    };*/
/*
    		$(document).ready(function(){
        	    $("#search-form").submit(function(){
        	    	var type = $('#type').val();
        	    	var brand = $('#brand').val();
        	    	var product = $('#product').val();
//         	    	var banner = $('#banner').val();
					var flag =0;
        			if( (type=='' || type==0) && (brand=='' || brand==0) && (product=='' || product==0)){
        				flag=1;
        			}
        			

        			if(flag){
            			alert('please select any one option');
            			return false;
        			}
        	    });
        	});*/

    	    
   /**  var map;
      function initialize() {
	    var pano;
		var latlng = new google.maps.LatLng(25.1943209,55.2742231);
		var panoOptions = {
		    position: latlng,
		    scrollwheel: true,
		    pov: {
			heading: 0,
			pitch: 0,
			zoom: 0,
		    }
		};
		map = new google.maps.StreetViewPanorama(
		    document.getElementById('map'), 
		    panoOptions);
		window.setInterval(function() {
		    var pov = map.getPov();
		    pov.heading += 0.2;
		    map.setPov(pov);
		}, 10);
//        var homeControlDiv = document.createElement("div");
 //       var homeControl = RenderButton(homeControlDiv, map,mapstate);
 //       homeControlDiv.index = 1;
 //       map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
      }
      
     google.maps.event.addDomListener(window, "load", initialize());*/


     function cityInfo1(reqData) {
         if (reqData == 0) {
            alert('please select category');
             return false
         }
         $("#brand").html('');
         $.ajax({
             type: "post",
             dataType: "json",
             url: baseurl + 'index.php/map/brandlist/' + reqData,
             success: function (request) {
                 if (request.status == 0) {
                 	alert('No brand available please select category');
                 } else {
                     $("#brand").select2({
                         data: request
                     });
                 }
             },
             error: function (data) {
                 alert("connection error");
                 location.reload();
             },
         });

         return false;
     }
</script>
    <!--<div  id="map1" style="display: block;width: 100%; height: 500px;" class="pano"></div>-->
  </body>
</html>
<script type="text/javascript">
var ROTATE_DEGS_PER_SEC = 60.0;           // Will take ~12 secs (360/60) for full revolution.
var ROTATE_DELAY_SECS = 2.5;              // Wait 3 seconds to start.
var ROTATE_FPS = 150.0;                   // Frames per second for rotate.
var panoLastTouchTimestamp = Date.now();  // Timestamp pano loaded in milliseconds.
var masterAnimate = true;                 // Set false to stop all animation.
var panoHeadingAngle = 0;                 // Current heading of pano.

  /*
   * Click the map to set a new location for the Street View camera.
   */

  var map;
  var panorama;

  function initMap() {
    var berkeley = {lat: 25.1943209, lng: 55.2742231};
    var sv = new google.maps.StreetViewService();

    panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));

    // Set up the map.
   /* map = new google.maps.Map(document.getElementById('map'), {
      center: berkeley,
      zoom: 16,
      streetViewControl: false
    });*/
    var markers = [];
    var map2 = new google.maps.Map(document.getElementById('map2'), {
      center: berkeley,
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var Latlng = new google.maps.LatLng(25.197193, 55.274321);
    markers = new google.maps.Marker({
  	  position: Latlng,
       map: map2,
       label:''
   });

    // Set the initial Street View camera to the center of the map
    sv.getPanorama({location: berkeley, radius: 50}, processSVData);

    // Look for a nearby Street View panorama when the map is clicked.
    // getPanoramaByLocation will return the nearest pano when the
    // given radius is 50 meters or less.
    map2.addListener('click', function(event) {
      sv.getPanorama({location: event.latLng, radius: 50}, processSVData);
    });
     panoLastTouchTimestamp = Date.now();


/*$("#pano").on("mouseenter", function () {
  masterAnimate = false;
  panoLastTouchTimestamp = 0;
});

$("#pano").on("mouseleave", function () { 
	masterAnimate = true;
	panoLastTouchTimestamp = 0;
});*/

	$("#pano").on("click", function () {
		panoLastTouchTimestamp = 0;
	  	if(masterAnimate==true){
	  		masterAnimate = false;
	  	}else{
	  		masterAnimate = true;
	  	}
	});

  
// Add animation which rotates our pano.
 window.setInterval(function() {
	 if (masterAnimate == false) {
	return;
  }
  if (Date.now() < panoLastTouchTimestamp + ROTATE_DELAY_SECS * 1000) {
	console.log(Math.floor(Date.now() / 1000));
	return;
  }
  var pov = panorama.getPov();
  pov.heading += ROTATE_DEGS_PER_SEC / ROTATE_FPS;
  panoHeadingAngle = pov.heading;
  panorama.setPov(pov);
}, 10000 / ROTATE_FPS);

// Add a click listener which delays rotation.
panorama.addListener('pov_changed', function() {
  // Check that change in POV is a user click, versus expected animation.
  var pov = panorama.getPov();
  if (panoHeadingAngle != pov.heading) {
	panoLastTouchTimestamp = Date.now();  // Will cease animation for a while.
  }
});
  
  }

  function processSVData(data, status) {
    if (status === 'OK') {
      var marker = new google.maps.Marker({
       // position: data.location.latLng,
        map: map,
        title: data.location.description
      });

      panorama.setPano(data.location.pano);
      panorama.setPov({
        heading: 360,
        pitch: 0
      });
      panorama.setVisible(true);

      marker.addListener('click', function() {
        var markerPanoID = data.location.pano;
        // Set the Pano to use the passed panoID.
        panorama.setPano(markerPanoID);
        panorama.setPov({
          heading: 360,
          pitch: 0
        });
        panorama.setVisible(true);
      });
    } else {
      console.error('Street View data not found for this location.');
      alert('Street View data not found for this location.');
    }
  }
  /*
var panorama;
function initialize(lat, lng) {
//		var lat = $('#lat').val();
//		var lng = $('#lng').val();
	//var latlng = $('#latlng').val();
	console.log( lat);
	console.log( lng);
    var location = new google.maps.LatLng(lat, lng);
    var panoramaOptions = {
        position: location,
        pov: {
        heading: 4,
            pitch: 10
        }
    };
    


    
    panorama = new  google.maps.StreetViewPanorama(document.getElementById('pano'),panoramaOptions);
    var move = true;
    $("#pano").on("mouseenter", function () {
        move = false;
    });

    $("#pano").on("mouseleave", function () {
        move = true;
    });
    

    var i = 0;
    window.setInterval(function () {
        panorama.setPov({
            heading: i,
            pitch: 10,
            zoom: 0
        });
        if (move) {
            i += 0.1;
        }
    }, 10);
}*/

google.maps.event.addDomListener(window, 'load', initMap);
</script>

<!-- Modal 
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information</h4>
        </div>
        <div class="modal-body" id="modal_content">
          <img src="<?php //echo base_url().'assets/img/loading.gif';?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>-->
  
<!-- Modal 
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enquiry</h4>
        </div>
        <div class="modal-body" id="modal_content1">			      
        <form method="POST" id="enquiry" action="">
          <div class="form-group row">
          <label for="example-name-input" class="col-xs-2 col-form-label">Name</label>
          <div class="col-xs-10">
            <input class="form-control" type="text" name="name" value="" id="example-name-input">
          </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-xs-2 col-form-label">Email</label>
            <div class="col-xs-10">
              <input class="form-control" type="email" name="email" value="" id="example-email-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-company-input" class="col-xs-2 col-form-label">Company</label>
            <div class="col-xs-10">
              <input class="form-control" type="text" name="company" value="" id="example-company-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="exampleTextarea" class="col-xs-2 col-form-label">Message</label>
            <div class="col-xs-10">
              <textarea class="form-control" name="message" id="exampleTextarea" rows="3"></textarea>
            </div>
          </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-success success" value="Submit">
        </div>
        </form>        </div>

      </div>
      
    </div>
  </div>-->
  <style>
.btn{  padding: 6px !important; }
/*
body { background:#000 !important; }
.size {
    background: #094590 none repeat scroll 0 0;
    color: #fff;
    padding: 5px;
    text-align: center;

}
.size_content {
    padding: 5px 30px;
    text-align: justify;
    text-overflow: ellipsis;
    width: 33%;
}

.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
   background-color: #fff;
    font-family: Roboto;
    font-size: 10px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 100% !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#pac-input1 {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input1:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}



</style>


<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
 /*var markers = [];
  var map2 = new google.maps.Map(document.getElementById('map2'), {
    center: {lat: 25.197193, lng: 55.274321},
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var Latlng = new google.maps.LatLng(25.197193, 55.274321);
  markers = new google.maps.Marker({
	  position: Latlng,
     map: map2,
     label:''
 });

  google.maps.event.addListener(map2, "click", function (e) {
// 		$('#cycle').hide();
// 		$('#pano').show();
	     
	    //lat and lng is available in e object
	    var latLng = "'"+e.latLng+"'";
	 result = latLng.split(",");
	var result1 = result[0].split("("); 
	 var result2 = result[1].split(")"); 
//		 console.log(result1[1]);
//		 console.log(result2[0]);
//		$('#lat').val($.trim(result1[1]));
//		$('#lng').val($.trim(result2[0]));
	//  var location = new google.maps.LatLng(latLng);
// 	console.log("LatLong: "+e.latLng);
	var latitude = parseFloat(result1[1]);
		var longitude = parseFloat(result2[0]);
//	  		$('#latlng').val(latLng);
	initialize(latitude, longitude)

	});*/

  // Create the search box and link it to the UI element.
//   var input = document.getElementById('pac-input');
//   var searchBox = new google.maps.places.SearchBox(input);
  //map2.controls[google.maps.ControlPosition.TOP_CENTER].push(input);


  // Bias the SearchBox results towards current map's viewport.
//   map2.addListener('bounds_changed', function() {
//     searchBox.setBounds(map2.getBounds());
//   });


  
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
/*   searchBox.addListener('places_changed', function() {

    var places = searchBox.getPlaces();
console.log(places);

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    var LatLng ={};
 	// For each place, get the icon, name and location.
    places.forEach(function(place) {
	    
	 //	document.getElementById("lat").value = place.geometry.location.lat();
	 //	document.getElementById("lng").value = place.geometry.location.lng();
	 	LatLng = place.geometry.location.toJSON();
	 	console.log(LatLng);
	 	$("#map1").show();
		 mapview( LatLng );
		 minscreen();
		 google.maps.event.addDomListener(window, "load", initialize("Maximize"));
      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map2,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map2.fitBounds(bounds);
  }); */
  // [END region_getplaces]



</script>    
