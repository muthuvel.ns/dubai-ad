<?php 
$lat = (!empty($marker[0]['lat'])?$marker[0]['lat']:'25.1943209');
$lng = (!empty($marker[0]['lng'])?$marker[0]['lng']:'55.2742231');
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        overflow:hidden;
      }
       .gm-iv-address {
      display: none !important;
      }
   .gm-fullscreen-control{
      display: none !important;
      }
       .gm-compass{
      display: none !important;
      }
     .gmnoprint svg{
     display: none !important;
     }
    </style>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>assets/js/jquery.cyclotron.js"></script>
        <script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUlQEVELKOBQi39VWgb7jjjq4a4lq9jdU&libraries=places"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css';?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/select2/select2.min.css';?>">
    <script src="<?php echo base_url().'assets/js/select2/select2.min.js'?>"></script>
    <script>
    $(document).ready(function(){
		 $(".js-example-basic-single").select2();
		 $(".js-example-data-array-selected").select2();
		 $('.cycle').cyclotron();
		 $("#brand").select2({
	            data: <?php echo (!empty($brand) ? $brand : NULL) ?>
	       });
		// var lat = <?php //echo $lat;?>;
		// var lng = <?php //echo $lng;?>;

		 $("#type").val(<?php echo (!empty($requestData['type']) ? $requestData['type'] : 0); ?>).trigger("change");
		// $("#product").val(<?php //echo (!empty($requestData['product']) ? $requestData['product'] : 0); ?>).trigger("change");
		 $("#brand").val(<?php echo (!empty($requestData['brand']) ? $requestData['brand'] : 0); ?>).trigger("change");
		 $("#banner").val(<?php echo (!empty($requestData['banner']) ? $requestData['banner'] : 0); ?>).trigger("change");
	});
    </script>

  </head>
  <body  onload="init()">
  <input type="hidden" id="marker" value='<?php echo (!empty($marker)?json_encode($marker):''); ?>'/>
  <input type="hidden" id="baseurl" value="<?php echo base_url();?>"/>
  <input type="hidden" id="lat" value="<?php echo $lat;?>"/>
  <input type="hidden" id="lng" value="<?php echo $lng;?>"/>
  <div style="height:100%" >
  <div id="pano" style='background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%; '></div>
  <!-- <div id="cycle" class='cycle' style='display:none;background-image:url(<?php echo base_url().'assets/images/'.(!empty($marker[0]['address'])?$marker[0]['address']:'')?>); background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%;'>-->
  <div id="cycle" class='cycle' style='display:none;background-position: -4067.3560813933063px 50%;height: 100%;cursor: move;width:100%;background-size:100% 100%;'>
	<?php echo base_url().'assets/images/exe/1.exe';?>
	</div>
	<!-- Left content start -->	
    <div class="pull-left contrst topaction">
		<div class="title" >
		<div style='float:right;'><button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal1' title='Enquiry'><i class='glyphicon glyphicon-euro'></i></button>  <button type='button' class='btn btn-info btn-lg' id="modal" onclick='modalData("<?php echo (!empty($marker[0]['id'])?$marker[0]['id']:''); ?>")' title='Information'><i class='glyphicon glyphicon-info-sign'></i></button></div>
			<h1 id="title"><?php echo (!empty($marker[0]['name'])?$marker[0]['name']:''); ?>  </h1>
			<h5 id="description" style="text-align:justify;line-height: 1.5em;"> <?php echo (!empty($marker[0]['description'])?$marker[0]['description']:''); ?> </h5>
		</div>
    </div>
    
    <div class="left-bottom contrst " style="top:-10%;">
		<a href="<?php echo base_url().'index.php/map';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color:#fff">Home</a>
		<a href="<?php echo base_url().'index.php/logout';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5)  ;color:#fff">Logout</a>
		<!-- <a href="<?php //echo base_url().'index.php/map';?>" class="btn btn-default" style="background-color:rgba(255, 255, 255, 0.5);color:#fff">Back</a> -->
	</div>
    
	<!-- Left content End -->	
    
    <!-- Right content Start -->
    <div class="pull-right contrst topaction">
		<div class="options">
			
			<input type="hidden" id="lat" name="start_lat" value=""/>
			<input type="hidden" id="lng" name="start_lng" value=""/>
			<div id="map2" style=""></div>    
			<div class="block" id="map_form">
			<form id="search-form" method="post" action="" class="text_cont">
			  <div class="form-group col-md-6">
				<label for="location">Location</label>
				<select class="form-control js-example-basic-single" name="type" id="type">
				  <option value="0">-- Select --</option>
				  <option value="1">Sheikh Zayed Road</option>
				  <option value="2">JBR</option>
				  <option value="3">Burj Khalifa</option>
				  <option value="4">Diera Creek</option>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="product">Category</label>
				<select class="form-control js-example-basic-single" name="product" id="product" onchange="cityInfo1(this.value)">
				  <option value="0">-- Select --</option>
				  <option value="1">Fashion</option>
				  <option value="2">Sports</option>
				  <option value="3">Electronics</option>
				  <option value="4">Home Appliances</option>
				  <?php if ($requestData['product']) { ?>
                       <option value="<?php echo $requestData['product'] ?>" selected="selected"> <?php echo $productname ?> </option>
                  <?php } ?>
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="brand">Brand</label>
				<select class="form-control js-example-data-array-selected" name="brand" id="brand">
				  <option value="0">-- Select --</option>
				  <?php if ($requestData['brand']) { ?>
                       <option value="<?php echo $requestData['brand'] ?>" selected="selected"> <?php echo $brandname ?> </option>
                  <?php } ?>	
				</select>
			  </div>
			  <div class="form-group col-md-6">
				<label for="product">Type</label>
				<select class="form-control js-example-basic-single" name="banner" id="banner">
				  <option value="0">-- Select --</option>
				  <option value="1">Flex</option>
				  <option value="2">Big Screen</option>
				</select>
			  </div>
			  <div class="form-group col-md-offset-3 col-md-6">
			  	<input type="submit" id="button" value="Search" class="btn btn-default btn_search" style=" padding-top: 3px !important; width:100%; float:right;margin-top:20px;background-color:#fff;border:1px solid #fff;">
				 <!--button type="submit" class="btn btn-default btn_search" style=" padding-top: 3px !important;">Search</button--> 
			  </div>
			</form>

    </div>
    </div>
    </div>
    <script>
    	var baseurl = $("#baseurl").val();
    	var markerpoint;
    	function init(){
    	var map1 = new google.maps.Map(document.getElementById("map2"), {
    	    center: new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>),
    	    zoom: 16,
    	    mapTypeId: 'roadmap'
    	  });

    	if($('#marker').val()!=''){console.log($('#marker').val());
    	    markerpoint    =   JSON.parse($('#marker').val());
    	  }
		console.log(markerpoint);
    	var infoWindow = new google.maps.InfoWindow;
    	var lat_lng = new Array();
    	// Change this depending on the name of your PHP file
//    	 	$.post(baseurl+"index.php/map/viewmaps?type=1", function(data) {

//    	 	var markerpoint    =  jQuery.parseJSON(data);
    	     /** set marker point */
    		        var point1;                              
    	  		$.each(markerpoint, function(i, point1){
    	  	  		var name = point1.name;
    		  	  	var address = point1.address;
    		  	  var id = point1.id;
    		  	  var description = point1.description;

    		  	 	var imageurl = baseurl+"assets/images/exe/1.exe";
    		  	 	if(i/2==0){
    		  	 		var imageurl = baseurl+"assets/images/exe/2.exe";
    		  	 	}
    		  		var html = imageurl;//"<b>" + name + "</b><b><div style='float:right;'><button type='button' class='btn btn-info btn-lg'  data-toggle='modal' data-target='#myModal' title='Enquiry'><i class='glyphicon glyphicon-euro'></i></button>  <button type='button' class='btn btn-info btn-lg' onclick='modalData("+id+")' title='Information'><i class='glyphicon glyphicon-info-sign'></i></button></div></b> <br><br><br> <div class='cycle' style='background-image:url("+imageurl+"); background-position: -4067.3560813933063px 50%;height: 366px;cursor: move;width:654px;'></div>";
    				var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
    				lat_lng.push(myLatlng);
    				var marker = new google.maps.Marker({
    				  position: myLatlng,
    				      map: map1,
    				      icon: baseurl+"assets/img/flag1.png",
    				   //    label: 's',
    				 //     title: mar
    				});
    				bindInfoWindow(marker, map1, infoWindow, html, description, name, id);
    	  		});
    	  		google.maps.event.addListener(map1, "click", function (e) {
    	  			$('#cycle').hide();
    	  			$('#pano').show();
       		     
    	  		    //lat and lng is available in e object
    	  		    var latLng = "'"+e.latLng+"'";
    	  		 result = latLng.split(",");
    	  		var result1 = result[0].split("("); 
    	  		 var result2 = result[1].split(")"); 
//     	  		 console.log(result1[1]);
//     	  		 console.log(result2[0]);
//     	  		$('#lat').val($.trim(result1[1]));
//     	  		$('#lng').val($.trim(result2[0]));
    	  		//  var location = new google.maps.LatLng(latLng);
    	  		console.log("LatLong: "+e.latLng);
    	  		var latitude = parseFloat(result1[1]);
     	  		var longitude = parseFloat(result2[0]);
    //	  		$('#latlng').val(latLng);
//     	  		initialize(latitude, longitude)
    	  		google.maps.event.addDomListener(window, 'load', initialize(latitude,longitude));
    	  		});
//    	 	});
    	}
    		function bindInfoWindow(marker, map1, infoWindow, html, description, name, id) {
    		      google.maps.event.addListener(marker, 'click', function() {
    		      //  infoWindow.setContent(html);
    		     //   infoWindow.open(map1, marker);console.log(html);
    		     $('#pano').hide();
    		     $('#cycle').show();
    		        document.getElementById("cycle").style.backgroundImage = "url('"+html+"')";
    		        $('#modal').attr('onClick','modalData('+id+')');
    		        $('#description').html(description);
    		        $('#title').html(name);
    		        $('.cycle').cyclotron();
    		      });
    		}

    		function modalData(reqId){
    			$('#myModal').modal('show');
    			$.post(baseurl+"index.php/map/modaldata?type="+reqId, function(data) {
    				$("#modal_content").html(data);
    				
    			});
    		 }

    		var panorama;
    		function initialize(lat, lng) {
//     			var lat = $('#lat').val();
//     			var lng = $('#lng').val();
    			//var latlng = $('#latlng').val();
    			console.log( lat);
    			console.log( lng);
    		    var location = new google.maps.LatLng(lat, lng);
    		    var panoramaOptions = {
    		        position: location,
    		        pov: {
    		        heading: 4,
    		            pitch: 10
    		        }
    		    };
    		    panorama = new  google.maps.StreetViewPanorama(document.getElementById('pano'),panoramaOptions);
    		    var move = true;
    		    $("#pano").on("mouseenter", function () {
    		        move = false;
    		    });

    		    $("#pano").on("mouseleave", function () {
    		        move = true;
    		    });

    		    var i = 0;
    		    window.setInterval(function () {
    		        panorama.setPov({
    		            heading: i,
    		            pitch: 10,
    		            zoom: 0
    		        });
    		        if (move) {
    		            i += 0.2;
    		        }
    		    }, 10);
    		}

    		google.maps.event.addDomListener(window, 'load', initialize(<?php echo $lat;?>,<?php echo $lng;?>));


    		function cityInfo1(reqData) {
                if (reqData == 0) {
                   alert('please select category');
                    return false
                }
                $("#brand").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/map/brandlist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                        	alert('No brand available please select category');
                        } else {
                            $("#brand").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {
                        alert("connection error");
                        location.reload();
                    },
                });

                return false;
            }
    </script>
    <!-- <div  id="map1" style="display: block;width: 100%; height: 500px;" class="pano"></div> -->
  </body>
</html>

<!-- Modal -->
  <div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information</h4>
        </div>
        <div class="modal-body" id="modal_content">
          <img src="<?php echo base_url().'assets/img/loading.gif';?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- Modal -->
  <div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enquiry</h4>
        </div>
        <div class="modal-body" id="modal_content1">			      
        <form method="POST" id="enquiry" action="">
          <div class="form-group row">
          <label for="example-name-input" class="col-xs-2 col-form-label">Name</label>
          <div class="col-xs-10">
            <input class="form-control" type="text" name="name" value="" id="example-name-input">
          </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-xs-2 col-form-label">Email</label>
            <div class="col-xs-10">
              <input class="form-control" type="email" name="email" value="" id="example-email-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-company-input" class="col-xs-2 col-form-label">Company</label>
            <div class="col-xs-10">
              <input class="form-control" type="text" name="company" value="" id="example-company-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="exampleTextarea" class="col-xs-2 col-form-label">Message</label>
            <div class="col-xs-10">
              <textarea class="form-control" name="message" id="exampleTextarea" rows="3"></textarea>
            </div>
          </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-success success" value="Submit">
        </div>
        </form>        
        </div>
      </div>
    </div>
  </div>
  <style>
.btn{  padding: 6px !important; }
body { background:#000 !important; }
.size {
    background: #094590 none repeat scroll 0 0;
    color: #fff;
    padding: 5px;
    text-align: center;

}
.size_content {
    padding: 5px 30px;
    text-align: justify;
    text-overflow: ellipsis;
    width: 33%;
}

.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
   background-color: #fff;
    font-family: Roboto;
    font-size: 10px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 100% !important;
}

#pac-input:focus {
  border-color: #4d90fe;
}

#pac-input1 {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 300px;
}

#pac-input1:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

</style>

