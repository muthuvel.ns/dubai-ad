<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	function __construct()
	 {
	   parent::__construct();
	   $this->load->helper(array('url','language'));
	   $this->load->library('session');
	   $this->load->model('Login_model', '', TRUE);
	   
	   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	   header("Pragma: no-cache"); // HTTP 1.0.
	   header("Expires: 0"); // Proxies.
		if (!isset($this->session->userdata['log_in']) || empty($this->session->userdata['log_in'])) {
            redirect(base_url());
        }
	}
	
	public function index()	{
		$this->load->view('map/index');
	}
	public function viewmaps(){
		$type = (!empty($_GET['type'])?$_GET['type']:'');
		
		$userdata = $this->Login_model->markers($type);
		$i=0;
		foreach ( $userdata as $row ){
		 $response[$i]['name']		= $row['name'];
		 $response[$i]['address']	= $row['address'];
		 $response[$i]['lat']		= $row['lat'];
		 $response[$i]['lng']		= $row['lng'];
		 $response[$i]['type']		= $row['type'];
		 $response[$i]['id']		= $row['id'];
		  $i++;
		}
		
		// End XML file
		echo json_encode($response);exit;
	}
	
	public function mapview(){//echo ASSETPATH;exit;
		$response = '';
		if (empty( $_REQUEST )){
			redirect(base_url());
		} 
		//echo '<pre>';print_r($_REQUEST);exit;
 		$requestData = $_REQUEST;
 		$data['requestData'] = $requestData;
// 		$requestData['type'] =1;
// 		$requestData['brand'] =0;
// 		$requestData['product'] =0;
		
		$userdata = $this->Login_model->getNearestMarkersLocationList( $requestData );
		//echo '<pre>';print_r($userdata);exit;
		$marker=array();
		if ( !empty( $userdata )){
			$i=0;
			foreach ( $userdata as $row ){
				if ($row['type'] == 1 ){
				//Sheikh Zayed Road
				$marker['lat'] ='25.062717';
				$marker['lng'] ='55.130760';
				}elseif ($row['type'] == 2 ){
				//JBR
				$marker['lat'] ='25.076944';
				$marker['lng'] ='55.134136';
				}elseif ($row['type'] == 3 ){
				//Burj Khalifa
				$marker['lat'] ='25.1943209';
				$marker['lng'] ='55.2742231';
				}elseif ($row['type'] == 4 ){
				//Deira Creek
				$marker['lat'] ='25.271564';
				$marker['lng'] ='55.306042';
				}
				$response[$i]['name']		= $row['name'];
				$response[$i]['address']	= $row['address'];
				$response[$i]['lat']		= $row['lat'];
				$response[$i]['lng']		= $row['lng'];
				$response[$i]['type']		= $row['type'];
				$response[$i]['id']			= $row['id'];
				$response[$i]['description']= $row['description'];
				$flagurl = base_url()."assets/img/flag1.png";
				if ( $row['banner'] == 1 ){
					$flagurl = base_url()."assets/img/flag2.png";
				}
				$response[$i]['flagurl']		=$flagurl;
				$i++;
			}
		}
		$data['latlong'] = $marker;
		if (!empty( $requestData['product'] )) {
			if ( $requestData['product'] == 1 ){
				$cityInfo = array( '1'=>'prada','2'=>'boss');
			}elseif ( $requestData['product'] == 2 ){
				$cityInfo = array( '1'=>'Adidas','2'=>'Reebok');
			}elseif ( $requestData['product'] == 3 ){
				$cityInfo = array( '1'=>'LG','2'=>'SUMSUNG');
			}elseif( $requestData['product'] == 4 ){
				$cityInfo = array( '1'=>'Bosch','2'=>'Haier');
			}
		}
		
		if (!empty($cityInfo)) {
			$city[] = array('id' => '0', 'text' => '-- select brand --');
			foreach ($cityInfo as $key => $val) {
				if ( $requestData['brand'] == $key){
					$data['brandname'] = $val;
				}
				$city[] = array('id' => $key, 'text' => $val);
			}
		} else {
			$city = array('status' => 0);
		}
		
		$product = array();
		$productInfo = array( '1'=>'Fashion','2'=>'Sports','3'=>'Electronics','4'=>'Home Appliances');
		
		foreach ($productInfo as $key1 => $val1) {
			if ( $requestData['product'] == $key1){
				$data['productname'] = $val1;
			}
			$product[] = array('id' => $key1, 'text' => $val1);
		}
		
		$data['brand']= json_encode($city);
		//echo '<pre>';print_r($data);exit;
		
		$data['marker']=$response;
 //		echo '<pre>';print_r($data);exit;
// 		$this->load->view('map/viewmaps');
		$this->load->view('map/mapview', $data);
	}
	
	public function modaldata(){
		//echo $_GET['type']; exit;
		$requestData['id'] = $_REQUEST['type'];

		$userdata = $this->Login_model->getNearestMarkersLocationList( $requestData ); 
		
		$html = '<div class="container">
            <div class="form-group row">
            <label class="col-md-2 col-form-label size">Visual size</label>
            <div class="col-md-10 size_content"> 185M X 25M</div>
            </div>

            <div class="form-group row">
            <label class="col-md-2 col-form-label size">Type</label>
            <div class="col-md-10 size_content"> Window Graphic</div>
            </div>

            <div class="form-group row">
            <label class="col-md-2 col-form-label size">Location</label>
            <div class="col-md-10 size_content">' .$userdata[0]['name']. '</div>
            </div>

            <div class="form-group row">
            <div class="col-md-offset-1 col-md-10 col-md-offset-1 size_content"> Traffic flow of <b>3,50,000 cars</b> per day </div>
            </div>
    
            <div class="form-group row">
            <label class="col-md-2 col-form-label size">Description</label>
            <div class="col-md-10 size_content">' .$userdata[0]['description']. '</div>
            </div>

            <div class="form-group row">
            <label class="col-md-2 col-form-label size">Slots Status </label>
            <div class="col-md-10 size_content"> Completed! </div>
            </div>

          </div>';
		echo $html;
	}
	
	public function getlocation(){
		$response = '';
		if (empty( $_REQUEST )){
			echo json_encode($response);
		}
		//echo '<pre>';print_r($_REQUEST);exit;
		$requestData = $_REQUEST;
		
		$userdata = $this->Login_model->getNearestMarkersLocationList( $requestData );
		if ( !empty( $userdata )){
			$i=0;
			foreach ( $userdata as $row ){
				$response[$i]['name']		= $row['name'];
				$response[$i]['address']	= $row['address'];
				$response[$i]['lat']		= $row['lat'];
				$response[$i]['lng']		= $row['lng'];
				$response[$i]['type']		= $row['type'];
				$response[$i]['id']			= $row['id'];
				$i++;
			}
		}
		
		echo json_encode($response);exit;
		
	}
	
	public function brandlist( $brand) {
		if (!empty( $brand )) {
			if ( $brand == 1 ){
				$cityInfo = array( '1'=>'prada','2'=>'boss');
			}elseif ( $brand == 2 ){
				$cityInfo = array( '1'=>'Adidas','2'=>'Reebok');
			}elseif ( $brand == 3 ){
				$cityInfo = array( '1'=>'LG','2'=>'SUMSUNG');
			}elseif( $brand == 4 ){
				$cityInfo = array( '1'=>'Bosch','2'=>'Haier');
			}
		}
	
		if (!empty($cityInfo)) {
			$city[] = array('id' => '0', 'text' => 'select brand');
			foreach ($cityInfo as $key => $val) {
				$city[] = array('id' => $key, 'text' => $val);
			}
		} else {
			$city = array('status' => 0);
		}
		echo json_encode($city);
	}
	public function frame( ) {
			
		$this->load->view('map/win');
	}
}
