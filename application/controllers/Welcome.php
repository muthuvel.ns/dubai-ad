<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	 {
	   parent::__construct();
	   $this->load->helper(array('url','language'));
	   $this->load->library('session');
	   
	   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	   header("Pragma: no-cache"); // HTTP 1.0.
	   header("Expires: 0"); // Proxies.
	   if (isset($this->session->userdata['log_in']) || !empty($this->session->userdata['log_in'])) {
	   	redirect(base_url().'index.php/map');
	   }
	}
	
	public function index()
	{
		$this->data = '';
		$message = $this->session->flashdata('message');
		
		if ($message) {
			$this->data['message'] = $message;
		}
		$this->load->view('welcome/index.php', $this->data);
	}
}
