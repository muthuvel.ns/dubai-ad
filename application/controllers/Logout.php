<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logout extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        //$this->load->model('User_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->library('session');
        if (!isset($this->session->userdata['log_in']) || empty($this->session->userdata['log_in'])) {
            redirect(base_url());
        }
    }

    function index() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('userguid');
        $this->session->unset_userdata('log_in');
//  	$this->session->sess_destroy();
        redirect(base_url());
    }

}

?>