<?php
$lang['text_matrimony']						= 'Matrimony';
$lang['text_pwd']							= 'password';
$lang['text_reset_pwd']						= 'Reset Password';
$lang['text_success']						= 'Success';
$lang['text_success_add']					='Successfully added';
$lang['text_success_update']				= 'Sucessfully updated'; 
$lang['text_success_insert']				= 'Sucessfully inserted';
$lang['text_already_exists']				= 'Already exists';
$lang['text_invalid_email']					= 'Please enter valid email address';
$lang['text_invalid_uname_pwd']				= 'Username or Password you entered is incorrect';
$lang['text_mail']							= 'mail';
$lang['text_please_enter_email']			= 'Please enter the email';
$lang['text_please_enter_pwd']				= 'Please enter the password';
$lang['text_please_enter_email_pwd']		= 'Please enter the email and password';
$lang['text_please_activate_acc']			= 'Please activate your account';
$lang['text_email_not_exists']				= "Email id does't exists";
$lang['text_contact_admin']					= 'Please Contact System Administrator';
$lang['text_admin']							= 'Please Enter the correct email';
$lang['text_admin_only']					= 'You are not admin';
$lang['text_account_activate']				= 'Your account acctivated successfully';
$lang['text_success_pwd_reset']				= 'password reset successfully';
$lang['success_page_hi']					= 'Hi <b>%s </b>';
$lang['success_page_text_step1']			= 'To login, use  this  email - <b>%s </b>';
$lang['success_page_text_step2']			= 'Please check your <a href="http://gmail.com/">inbox or spam</a>. Confirmation mail has been sent to you.';
$lang['text_login'] 						= 'Login';
$lang['success_page_text_skip']				= 'Skip this page';
$lang['text_view_profile']					= 'View full profile';
$lang['text_view_all_matches']				= 'View all matches';
$lang['text_already_exits_email']			= 'User Already exists with same email';
$lang['text_profile_not']					= 'Profile details not fount';
$lang['text_add_profile_prefrence']			= 'Please add your profile prefrence';
$lang['text_alert_msg']						= 'Alert message';
// $lang['success_page_text_step3']	= 'your account acctivated successfully';
$lang['text_matrimony_title']				= 'Madurai Somaeswar Matrimony';
/**MAIL */
$lang['mail_sub_reset_pwd']					= 'Reset Password';
$lang['mail_reset_pwd_step1']				= 'Dear %s (M%s)';
$lang['mail_reset_pwd_step2']				= 'You have requested to reset your';
$lang['mail_reset_pwd_step3']				= 'Reset Password';
/* activate account*/
$lang['mail_activate_acc_step1']			= 'Dear %s (M%s)';
$lang['mail_activate_acc_step2']			= 'Thanks for registering with';
$lang['mail_activate_acc_step3']			= 'Activate Account';

$lang['mail_interest_step1']				= 'Dear %s (M%s)';
$lang['mail_interest_profile']				= 'is Interested in your profile.';
$lang['mail_communiaction']					= 'Say Yes to communicate further!';
$lang['mail_member_id']						= '(M%s)';
$lang['mail_content_data']					= '%s Yrs, %s Cms  |  Hindu  :  %s  |  Location : %s, %s  |  Education : %s | Occupation : %s';
$lang['mail_accept_profile']				= 'is Accepted in your request.';

$lang['mail_interest_sub']					= '%s, I have sent you an Interest';
$lang['mail_accepted_sub']					= '%s, I have accepted your Interest';
$lang['mail_not_interest_sub']				= '%s - %s has declined your interest';
$lang['mail_send_sub']						= '%s has sent you a message';
$lang['mail_send_step1']					= 'Hi %s,';
$lang['mail_send_step2']					= '%s has sent you a message.';
$lang['mail_send_step3']					= 'Read Now';

$lang['sms_text_sub']						= '%s. Click memberId <a href="http://www.maduraisomaeswarmatrimony.com/">(%s)</a> see the inforamation';

/* activate account*/
$lang['mail_matches_step1']					= 'Dear %s (M%s),';
$lang['mail_matches_title1']				= 'Your profiles based on Partner Preference';
$lang['mail_matches_title2']				= 'Your profiles based on Similar Matches Listed below';
$lang['mail_matches_title3']				= 'Your profiles based on New Matches';
$lang['mail_matches_title4']				= 'Listed below members are viewed your profile';
$lang['mail_matches_sub1']					= '%s - Recently updated profile';
$lang['mail_matches_sub2']					= '%s - Similar Matches based on your preference';
$lang['mail_matches_sub3']					= '%s - some one viewed your profile';
$lang['mail_matches_sub4']					= '%s - New Matches based on your preference';

/* Registration failed */
$lang['registration_failed']				=	'Registration failed please try afer some time';
$lang['photo_add_failed']					='Failed to add please try afer some time';
$lang['upload_success']						='upolad success';
$lang['upload_after_some_time'] 			='upload after some time';
$lang['only_allowed_to_upload']			    ='Only allowed to upload 5 images only';
$lang['request_empty']          			='Request empty';
$lang['invalid_user_please']				='Invalid user please contact admin';
/* Inbox */
$lang['text_interest_message']				=	'This member is interested in your profile. Say Yes to communicate further';
$lang['text_image_message']					=	'This member has requested to view your photo';
$lang['text_interest_accept_message']		=	'This member has accepted your interest ';
$lang['text_arranged']						=	'This member has Arranged your profile';
$lang['text_messages_not_found']			=	'Messages Not Found';
$lang['text_not_interest']					=	'This member has not interested your profile';
$lang['text_inbox_mail']					=	'This member has send you a mail';

/* Send */
$lang['text_send_interest']					=	'You have send the interested for this member  ';
$lang['text_send_view_image']				=	'You have send the requested to view photo'; 
$lang['text_send_interest_accept']			=	'You have accepted this member interest';
$lang['text_send_arranged']					=	'You have arranged this member profile';
$lang['text_send_mail']						=	'You Send the mail for this member ';
$lang['text_send_not_interest']				=	'You have not interested for this profile';

$lang['failure_msg']			  			='failure';
$lang['success_msg']			 		    ='success';
$lang['upload_failure']			 		    ='upolad failure';
$lang['connection_error']		 			='Connection error';
$lang['error_occur_contact_admin']			='Error occurred please try afer some time or contact administrator';
$lang['please_set_main_profile_picture']	='Please set main profile picture';
/* new matches page */
$lang['mail_send_successfully_text']						='Mail send Successfully';
$lang['are_you_sure_the _shorlisted']						='Are you sure cancle the shorlisted ?';
$lang['yes']												='Yes';
$lang['no']													='No';
$lang['detail_not_found']									='Details not fount';
$lang['matches_not_found']									='Matches not fount';
$lang['conversation_remove_text']							='Conversation removed successfully';
$lang['please_try_again_after_some_time']					='Please try again after some time';
$lang['please_select_age_to']								='Please select Age to option';
$lang['please_select_age_from']								='Please select Age from option';
/*setting*/
$lang['email_format_invalid']								='Email Format Invalid';
$lang['email_already_exits_please_change_new_email_id']		='email already exists please change new email-id';
$lang['failure_to_update_please_try_again']					='Failure to update please try again';
$lang['update_success_fully_use_this_email_id_to_login']	='Update success fully use this email id  to login';
$lang['password_required']									='password required';
$lang['password_is_wrong']									='password is wrong';
$lang['valid_email_member_to_member_communication']			='A valid e-mail id will be used to send you partner search mailers, member to member communication mailers and special offers.';
$lang['pls_provide_your_password_for_security']				='Please provide your password for security reasons';
$lang['deactive_successfully']								='Deactivate successfully';



?>
